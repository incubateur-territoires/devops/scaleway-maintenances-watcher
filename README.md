# Scaleway Maintenance Watcher

## Introduction

Le script `maintenance.ts` est une solution élaborée pour s'assurer que toute l'équipe technique soit notifiée des maintenances planifiées sur Scaleway, même si, initialement, seule la personne administrant le compte reçoit ces notifications. Il a été conçu pour être exécuté chaque jour via un job CI, permettant ainsi une vérification quotidienne des maintenances à venir. À chaque détection d'une maintenance planifiée, un ticket GitLab est créé, ce qui, à son tour, génère une notification sur Mattermost pour notre équipe.

## Fonctionnement

1. **Authentification** :
   Le script commence par s'authentifier auprès de l'API Scaleway en utilisant les credentials fournis.

2. **Récupération des informations de maintenance** :
   Il interroge ensuite l'API pour obtenir la liste des maintenances planifiées pour les services utilisés.

3. **Vérification des maintenances** :
   Il parcourt la liste des maintenances et vérifie si elles ont déjà été notifiées ou non.

4. **Création de Tickets GitLab** :
   Pour chaque maintenance non notifiée, un ticket GitLab est créé avec les détails de la maintenance.

5. **Notification Mattermost** :
   La création d'un ticket GitLab déclenche une notification sur Mattermost, alertant ainsi l'équipe des maintenances à venir.

## Configuration

### Variables d'Environnement

- `SCALEWAY_API_ENDPOINT` : L'URL de l'API Scaleway. Par défaut, elle est réglée sur "https://api.scaleway.com".
- `SCALEWAY_ACCESS_TOKEN` : Le token d'accès pour l'API Scaleway.
- `SCALEWAY_ORGANIZATION_ID` : L'ID de l'organisation au sein de Scaleway.
- `SCALEWAY_DEFAULT_ZONE` : La zone par défaut sur Scaleway, réglée par défaut sur "fr-par-1".
- `SCALEWAY_DEFAULT_REGION` : La région par défaut sur Scaleway, réglée par défaut sur "fr-par".

- `GITLAB_API_ENDPOINT` : L'URL de l'API GitLab. Par défaut, elle est réglée sur "https://gitlab.com/api/v4".
- `GITLAB_PROJECT_ID` : L'ID du projet GitLab où les tickets seront créés.
- `GITLAB_TOKEN` : Le token d'API de GitLab utilisé pour créer des tickets.
