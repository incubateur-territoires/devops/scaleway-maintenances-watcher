const SCALEWAY_API_ENDPOINT = Deno.env.get("SCALEWAY_API_ENDPOINT") ??
  "https://api.scaleway.com";
const SCALEWAY_ACCESS_TOKEN = Deno.env.get("SCALEWAY_ACCESS_TOKEN");
const SCALEWAY_ORGANIZATION_ID = Deno.env.get("SCALEWAY_ORGANIZATION_ID");
const SCALEWAY_DEFAULT_ZONE = Deno.env.get("SCALEWAY_DEFAULT_ZONE") ??
  "fr-par-1";
const SCALEWAY_DEFAULT_REGION = Deno.env.get("SCALEWAY_DEFAULT_REGION") ??
  "fr-par";

const GITLAB_API_ENDPOINT = Deno.env.get("GITLAB_API_ENDPOINT") ??
  "https://gitlab.com/api/v4";
const GITLAB_PROJECT_ID = Deno.env.get("GITLAB_PROJECT_ID");
const GITLAB_TOKEN = Deno.env.get("GITLAB_TOKEN");

const fetchWithToken = (url: string, params: Record<string, string> = {}) =>
  fetch(`${url}?${new URLSearchParams(params)}`, {
    headers: {
      "X-Auth-Token": SCALEWAY_ACCESS_TOKEN,
      "Content-Type": "application/json",
    },
  }).then(async (response) => {
    if (!response.ok) {
      throw new Error(
        `Failed to fetch from ${url}. Status: ${response.status} ; ${
          JSON.stringify(await response.json())
        }`,
      );
    }
    return response.json();
  });

async function createGitLabIssueIfNotExist(
  title: string,
  description = "",
  labels: string[] = ["devops", "scope::transverse", "priority::major", "status::todo"],
) {
  const existingIssues = await fetch(
    `${GITLAB_API_ENDPOINT}/projects/${GITLAB_PROJECT_ID}/issues?search=${
      encodeURIComponent(title)
    }`,
    {
      headers: {
        "Authorization": `Bearer ${GITLAB_TOKEN}`,
        "Content-Type": "application/json",
      },
    },
  ).then((res) => res.json());

  const issueExists = existingIssues.some((issue: object) =>
    issue.title === title
  );

  if (issueExists) {
    console.warn(`Issue with title ${title} already exists, ignoring…`);
    return;
  }

  const payload = {
    title: title,
    labels: labels.join(","),
    description: description,
    confidential: true, // To prevent resources names leak
    issue_type: "incident",
  };

  const response = await fetch(
    `${GITLAB_API_ENDPOINT}/projects/${GITLAB_PROJECT_ID}/issues`,
    {
      method: "POST",
      headers: {
        "Authorization": `Bearer ${GITLAB_TOKEN}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    },
  );

  if (response.ok) {
    console.info(`Issue with title ${title} has been created…`);
  } else {
    console.error(`Failed to create issue ${title}…`);
  }
}

const servers = (await Promise.all([
  fetchWithToken(
    `${SCALEWAY_API_ENDPOINT}/instance/v1/zones/${SCALEWAY_DEFAULT_ZONE}/servers`,
  ).then((data) => data.servers),
  fetchWithToken(
    `${SCALEWAY_API_ENDPOINT}/rdb/v1/regions/${SCALEWAY_DEFAULT_REGION}/instances`,
  ).then((data) => data.instances),
]))
  .reduce((acc, r) => acc.concat(r), [])
  .filter((s) => s.maintenances.length > 0)
  .map((s) => ({
    id: s.id,
    location: s.zone ?? s.region,
    name: s.name,
    project: s.project ?? s.project_id,
    organization: s.organization ?? s.organization_id,
    tags: s.tags,
    maintenances: s.maintenances,
  }));
const projects =
  (await fetchWithToken(`${SCALEWAY_API_ENDPOINT}/account/v2/projects`, {
    organization_id: SCALEWAY_ORGANIZATION_ID,
  }).then((data) => data.projects))
    .reduce((acc, p) => ({ ...acc, [p.id]: p.name }), {});

await servers
  .map((d) => ({
    title: `[Maintenance] ${d.name} (🗺️ ${d.location} ; 📋 ${
      projects[d.project]
    })`,
    description: d.maintenances.map((m) => `
> ${m.reason}

${
      m.starts_at
        ? ("Start at : " + new Date(m.starts_at).toLocaleDateString(undefined, {
          weekday: "long",
          year: "numeric",
          month: "long",
          day: "numeric",
        }))
        : ""
    }
    `).join("\n"),
  }))
  .map(async (s) => {
    await createGitLabIssueIfNotExist(s.title, s.description);
  });
